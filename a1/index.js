


/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	


let personalName = "Judy";
let firstName = "First Name: ";
console.log(firstName, personalName);

let lastName = "Sanogal";
let lastName2= 'Last Name:';
console.log(lastName2, lastName)


let myAge = 30;
let myAge1 = "Age: ";
console.log(myAge1, myAge)

let Hobbies =["BOOKS", "BIKES", "TRAVEL", "EAT"];
console.log("Hobbies:")
console.log(Hobbies);

let myAddress = {
	housenumber: 00,
	Street: "None",
	city: "Of Your Business",
	state: "Please",
}
console.log("Work Address:");
console.log(myAddress)

let fullName1= " Steve Rogers ";
console.log("My full name is:" + fullName1);

let age = 40;
console.log("My current age is: " + age );


let myFriends = "My Friends are:"
let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];	
console.log(myFriends,friends);


let profile = {
		username: "captain_america",
		fullName: "Steve Rogers",
 		age: 40,
 		isActive: false,

}

console.log("My Full Profile:")
console.log(profile);

let fullName3 = "Bucky Barnes";
console.log("My bestfriend is: " + fullName3);

const lastLocation = "Arctic Ocean";
console.log("I was found frozen in:");






