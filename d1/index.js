// This is statment 
console.log("Hello, Batch 244!")

console.log ("Hello World! ") ; 

console.
log
(
	"Hello Again!"
)


// Synthax: let/const variableName;

let myVariable;
console.log(myVariable);

// Initializing variables
// Synthax: let/const variableName = value;

let productName = 'desktop computer';
console.log(productName);

let productPrice =18999;
console.log(productPrice);


const interest = 3.539;
console.log(interest);

// Reaasigning value
// synthax: variableName = newValue;


productName = "Laptop";
console.log(productName);


let friend = "Kate";
console.log(friend);
friend = "Jane"
console.log(friend);


let friend1 = "mark"
console.log(friend1)


// let/const local/global scope

let outerVariable = "Hello";
{
	let innerVariable = "Hello Again"
	console.log(innerVariable);
}

console.log(outerVariable);

// multiple variable declatration

let productCode = "DC017";
const productBrand = "Dell";

console.log(productCode,productBrand);

const greeting = "Hi";
console.log(greeting);



// [section Data Type]

let country = "Philippines";
let province = "Batangas";
console.log(country, province); 


let fullAddress = province + "," + country;
console.log (fullAddress);


let greeting2 = "I live the " + country;
console.log(greeting2);


let message = 'John\'s employee went home early.';
console.log(message);


//numbers and intgegers/wholenumbers 
let headCunt =26;
console.log(headCunt);

//exponential notation
let grade =98.7
console.log(grade);

//exponential notation
let planetDistance =2e10
console.log(planetDistance);

//combaning string and number

console.log("John's grade last quarter is " + grade);



//Bolean value true and false 

let isMarried = false;
let inGoodConduct =true;

console.log("isMarried: " + isMarried);
console.log("inGoodConduct; " + inGoodConduct);


//Arrays
//synthax: arrayName = [Element, element a,]

let grades =[98, 92, 90, 94];
console.log(grades);

//1 data type in arrayName
let details = ["John", "Smith", 32, true];
console.log(details);


//objects
//synthax let/const objectName = {PropertyA: value, }

// let person = {
// 	fullNAme: "John Smith"
// 	age: 32,
// 	isMarried: true,
// 	contact: ["0919777876", "0919876657"]
// 	address: {
// 		houseNumber: "345"
// 		city: "Manila"
// 	}
// }


let myGrades = {
	firstGrading: 98,
	secondGrading: 92,
	thirdGrading: 90,
	fourthGrading: 94
}

console.log(myGrades)



// interest = 4.490;
// console.log(interest);